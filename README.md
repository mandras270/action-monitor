# README #

### Project requirements ###

* Maven 3+
* Java 1.8+

### How to run the project? ###

* Just run the following command in your command prompt: **mvn tomcat7:run**

### What endpoints does the app provide? ###

* http://localhost:9090/
* http://localhost:9090/status

### List of known issues ###

* You can create an empty message
* You can update a message with an empty message
* You can update a message without an id (new message will be created)
* You can update a message using a non existing id (new message will be created)
