package com.bvtech.action.monitor.service;

import com.bvtech.action.monitor.dao.MessageRepository;
import com.bvtech.action.monitor.dao.entity.MessageEntity;
import com.bvtech.action.monitor.domain.InboundMessage;
import com.bvtech.action.monitor.domain.Operation;
import com.bvtech.action.monitor.domain.OutboundMessage;
import com.bvtech.action.monitor.service.transform.MessageTransformer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

public class MessageServiceTest {

    @Mock
    private MessageRepository messageRepository;

    @Mock
    private MessageTransformer messageTransformer;

    @InjectMocks
    private MessageService underTest;

    @Mock
    private InboundMessage inboundMessageMock;

    @Mock
    private MessageEntity messageEntityMock;

    @Mock
    private OutboundMessage outboundMessageMock;

    @Before
    public void setUp() {
        underTest = new MessageService();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSave() {
        //GIVEN
        when(inboundMessageMock.getOperationType()).thenReturn(Operation.INSERT);
        when(messageTransformer.transform(inboundMessageMock)).thenReturn(messageEntityMock);
        when(messageRepository.save(messageEntityMock)).thenReturn(messageEntityMock);
        when(messageTransformer.transform(eq(messageEntityMock), eq(Operation.INSERT))).thenReturn(outboundMessageMock);

        //WHEN
        final OutboundMessage actual = underTest.save(inboundMessageMock);

        //THEN
        Assert.assertEquals(outboundMessageMock, actual);
    }

}