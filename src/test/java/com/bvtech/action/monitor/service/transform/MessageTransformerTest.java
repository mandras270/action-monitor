package com.bvtech.action.monitor.service.transform;

import com.bvtech.action.monitor.dao.entity.MessageEntity;
import com.bvtech.action.monitor.domain.InboundMessage;
import com.bvtech.action.monitor.domain.Operation;
import com.bvtech.action.monitor.domain.OutboundMessage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.mockito.Mockito.when;

public class MessageTransformerTest {

    private MessageTransformer underTest;

    @Mock
    private InboundMessage inboundMessageMock;

    @Mock
    private MessageEntity messageEntityMock;

    @Before
    public void setUp() {
        underTest = new MessageTransformer();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testTransformWithInboundMessage() {
        //GIVEN
        when(inboundMessageMock.getMessage()).thenReturn("Your random name is ...");
        when(inboundMessageMock.getId()).thenReturn(6L);
        final MessageEntity expected = new MessageEntity(6L, "Your random name is ...");

        //WHEN
        final MessageEntity actual = underTest.transform(inboundMessageMock);

        //THEN
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testTransformWithMessageEntity() {
        //GIVEN
        final LocalDateTime now = LocalDateTime.now();
        when(messageEntityMock.getId()).thenReturn(7L);
        when(messageEntityMock.getMessage()).thenReturn("25 Random Thoughts That Will Make You Question Everything");
        when(messageEntityMock.getTimestamp()).thenReturn(now);
        final OutboundMessage expected = new OutboundMessage.Builder()
                .setOperation(Operation.UPDATE)
                .setId(7L)
                .setMessage("25 Random Thoughts That Will Make You Question Everything")
                .setTimestamp(now.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME))
                .build();

        //WHEN
        final OutboundMessage actual = underTest.transform(messageEntityMock, Operation.UPDATE);

        //THEN
        Assert.assertEquals(expected, actual);
    }
}