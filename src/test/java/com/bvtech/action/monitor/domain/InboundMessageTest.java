package com.bvtech.action.monitor.domain;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class InboundMessageTest {

    private InboundMessage underTest;

    @Test
    public void testGetOperationTypeWhenOperationIsInsert() {
        //GIVEN
        underTest = new InboundMessage("", null);

        //WHEN
        final Operation actual = underTest.getOperationType();

        //THEN
        Assert.assertEquals(Operation.INSERT, actual);
    }

    @Test
    public void testGetOperationTypeWhenOperationIsUpdate() {
        //GIVEN
        underTest = new InboundMessage("", 5L);

        //WHEN
        final Operation actual = underTest.getOperationType();

        //THEN
        Assert.assertEquals(Operation.UPDATE, actual);
    }
}