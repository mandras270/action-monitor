$(function() {
    var socket = new SockJS('/subscribe');
    var stompClient = Stomp.over(socket);
    stompClient.connect({}, function(frame) {
        $('#status').append('Websocket connection established!');
        stompClient.subscribe('/topic/messages', function(data) {
            var object = JSON.parse(data.body);
            $('#messages').append('<div>Message {' + object.message + '} was added with id {' + object.id + '} using operation ' + object.operation + ' at ' + object.timestamp + '</div>');
        });
    });

    $('#sendMessage').click(function() {
        stompClient.send("/app/subscribe", {}, JSON.stringify({
            'message': $('#text').val()
        }));
        $('#text').val('');
    });

    $('#updateMessage').click(function() {
        stompClient.send("/app/subscribe", {}, JSON.stringify({
            'message': $('#updatedText').val(),
            'id': $('#messageId').val()
        }));
        $('#updatedText').val('');
        $('#messageId').val('');

    });

});