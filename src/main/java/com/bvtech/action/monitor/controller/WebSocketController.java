package com.bvtech.action.monitor.controller;

import com.bvtech.action.monitor.domain.InboundMessage;
import com.bvtech.action.monitor.domain.OutboundMessage;
import com.bvtech.action.monitor.service.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class WebSocketController {

    private static final Logger LOG = LoggerFactory.getLogger(WebSocketController.class);

    @Autowired
    private MessageService messageService;

    @MessageMapping("/subscribe")
    @SendTo("/topic/messages")
    public OutboundMessage send(final InboundMessage inboundMessage) {
        LOG.debug("Received inboundMessage {}", inboundMessage);
        return messageService.save(inboundMessage);
    }

}
