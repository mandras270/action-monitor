package com.bvtech.action.monitor.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LandingPageController {

    public static final String REQUEST_MAPPING = "/";

    @RequestMapping(value = REQUEST_MAPPING)
    public String welcome() {
        return "index";
    }

}
