package com.bvtech.action.monitor.service.transform;

import com.bvtech.action.monitor.dao.entity.MessageEntity;
import com.bvtech.action.monitor.domain.InboundMessage;
import com.bvtech.action.monitor.domain.Operation;
import com.bvtech.action.monitor.domain.OutboundMessage;
import org.springframework.stereotype.Service;

import java.time.format.DateTimeFormatter;

@Service
public class MessageTransformer {

    public MessageEntity transform(final InboundMessage inboundMessage) {
        return new MessageEntity(inboundMessage.getId(), inboundMessage.getMessage());
    }

    public OutboundMessage transform(final MessageEntity messageEntity, final Operation operation) {
        return new OutboundMessage.Builder()
                .setOperation(operation)
                .setId(messageEntity.getId())
                .setMessage(messageEntity.getMessage())
                .setTimestamp(messageEntity.getTimestamp().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME))
                .build();
    }
}
