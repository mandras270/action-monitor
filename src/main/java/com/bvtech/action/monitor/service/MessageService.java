package com.bvtech.action.monitor.service;

import com.bvtech.action.monitor.dao.MessageRepository;
import com.bvtech.action.monitor.dao.entity.MessageEntity;
import com.bvtech.action.monitor.domain.InboundMessage;
import com.bvtech.action.monitor.domain.OutboundMessage;
import com.bvtech.action.monitor.service.transform.MessageTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessageService {

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private MessageTransformer messageTransformer;

    public OutboundMessage save(final InboundMessage inboundMessage) {
        final MessageEntity messageEntity = messageRepository.save(messageTransformer.transform(inboundMessage));
        return messageTransformer.transform(messageEntity, inboundMessage.getOperationType());
    }
}
