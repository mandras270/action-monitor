package com.bvtech.action.monitor.domain;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public class OutboundMessage {

    private Long id;
    private String message;
    private String timestamp;
    private Operation operation;

    private OutboundMessage() {
    }

    public Long getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public Operation getOperation() {
        return operation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OutboundMessage that = (OutboundMessage) o;
        return Objects.equal(id, that.id) &&
                Objects.equal(message, that.message) &&
                Objects.equal(timestamp, that.timestamp) &&
                operation == that.operation;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, message, timestamp, operation);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("message", message)
                .add("timestamp", timestamp)
                .add("operation", operation)
                .toString();
    }

    public static class Builder {

        private Long id;
        private String message;
        private String timestamp;
        private Operation operation;

        public Builder setId(final Long id) {
            this.id = id;
            return this;
        }

        public Builder setMessage(final String message) {
            this.message = message;
            return this;
        }

        public Builder setTimestamp(final String timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public Builder setOperation(Operation operation) {
            this.operation = operation;
            return this;
        }

        public OutboundMessage build() {
            final OutboundMessage result = new OutboundMessage();
            result.id = id;
            result.message = message;
            result.timestamp = timestamp;
            result.operation = operation;
            return result;
        }
    }
}
