package com.bvtech.action.monitor.domain;

public enum Operation {
    INSERT, UPDATE
}
