package com.bvtech.action.monitor.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;


public class InboundMessage {

    private Long id;
    private String message;

    @JsonCreator
    public InboundMessage(@JsonProperty("message") final String message, @JsonProperty("id") final Long id) {
        this.message = message;
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public Long getId() {
        return id;
    }

    public Operation getOperationType() {
        return id == null ? Operation.INSERT : Operation.UPDATE;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InboundMessage that = (InboundMessage) o;
        return Objects.equal(id, that.id) &&
                Objects.equal(message, that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, message);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("message", message)
                .toString();
    }
}
