package com.bvtech.action.monitor.dao;

import com.bvtech.action.monitor.dao.entity.MessageEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageRepository extends JpaRepository<MessageEntity, Long> {
}
